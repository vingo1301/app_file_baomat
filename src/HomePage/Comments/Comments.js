import React, { useState } from 'react'
import CloseIcon from '@mui/icons-material/Close';
import { Avatar } from '@mui/material';
import { getComment } from '../../Services/CommentService/CommentService';
import moment from 'moment/moment';
import InputEmoji from "react-input-emoji";
import SendIcon from '@mui/icons-material/Send';
import { useDispatch, useSelector } from 'react-redux';
import { setShowComment } from '../../Redux-toolkit/reducer/UserSlice';

export default function Comments() {
    let dispatch = useDispatch();
    let showComment = useSelector((state) => state.UserSlice.showComment);
    let fileId = useSelector((state) => state.UserSlice.fileId);

    const [text, setText] = useState("");
    // xử lý click outside
    const useOutsideAlerter = (ref) => {
        React.useEffect(() => {
          /**
           * Alert if clicked on outside of element
           */
          function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                dispatch(setShowComment(false))
            }
          }
          // Bind the event listener
          document.addEventListener("mousedown", handleClickOutside);
          return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }
    const wrapperRef = React.useRef(null);
    useOutsideAlerter(wrapperRef);
    // 
    let handleOnEnter = () => {
        // gọi api comment ở đây
    }

    let renderComments = () => {
        return getComment?.map((comment,index) => {
            return <div className='px-3 py-2 flex justify-start items-center'>
            <div style={{width:"4rem",height:"4rem"}}>
                <Avatar style={{width:"100%",height:"100%",objectFit:"cover"}} alt="Remy Sharp" src={comment?.user?.user_avatar} />
            </div>
            <div className='bg-white p-2 w-full ml-3 rounded-md text-left'>
                <p className='text-lg font-semibold'>{comment?.user?.user_fullname}</p>
                <p className='text-sm text-gray-500'>{moment(comment?.comment_time).format("HH:mm, DD/MM/YYYY")}</p>
                <p className=''>{comment?.comment_content}</p>
            </div>
        </div>
        })
    }
  return (
    <div ref={wrapperRef} id='comments' className={`absolute left-0 bottom-0 z-50 h-4/6 bg-gray-200 overflow-hidden w-full flex flex-col ${!showComment&&"hidden"}`}>
        <div style={{borderTop:"1px solid #d5d5d5"}} className='bg-white p-2 flex justify-between'>
            <div></div>
            <p className='text-sky-500 text-lg font-semibold'>Nhận Xét</p>
            <button onClick={() => dispatch(setShowComment(false))}><CloseIcon></CloseIcon></button>
        </div>
        <div className='overflow-y-scroll h-full'>
            {renderComments()}
        </div>
        <div className='bg-white flex'>
            <InputEmoji value={text} onChange={setText} cleanOnEnter 
            onEnter={handleOnEnter} placeholder="Nhập bình luận..." />
            <button className='pr-4 text-blue-500'><SendIcon></SendIcon></button>
        </div>
    </div>
  )
}
