import React, { useState } from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { Input } from 'antd';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function DoiMatKhau() {
    let navigate= useNavigate();
    let user = useSelector((state) => state.UserSlice.user);
    let [waring1,setWarning1] = useState("");
    let [waring2,setWarning2] = useState("");
    let [oldPassword,setOldPassword] = useState("");
    let [password,setPassword] = useState("");
    let [password2,setPassword2] = useState("");
    let handleUpdatePass = () => {
        if(oldPassword.length < 8){
            toast.error("Vui Lòng Nhập Mật Khẩu Cũ !", {
                position: toast.POSITION.TOP_CENTER
              });return;
        }
        if(password.length < 8){
            setWarning1("Mật Khẩu Phải Đủ 8 Ký Tự!");
            return;
        }
        let valid = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/
        if(!valid.test(password)){
            setWarning1("Mật Khẩu Không Hợp Lệ!");
            return;
        }
        if(password !== password2){
            setWarning1("Mật Khẩu Không Khớp!");
            setWarning2("Mật Khẩu Không Khớp!");
            return;
        }
        setWarning1("");
        setWarning2("");
        // gọi api đổi mk ở đây
    }
  return (
    <div id='changePassword' className='h-screen bg-gray-200'>
        <div>
            <button onClick={() => navigate(-1)} className='w-full text-left pl-4 text-white bg-gradient-to-r from-cyan-500 to-blue-500 text-2xl flex items-center py-2'><ArrowBackIosIcon className='mr-2'></ArrowBackIosIcon>  Đổi Mật Khẩu</button>
        </div>
        <div className='p-2 leading-8 text-lg'>
            <p>Mật khẩu mới không được trùng với mật khẩu cũ.</p>
            <p>Mật khẩu phải dài ít nhất 8 ký tự. Trong đó có ít nhất 1 ký tự in hoa, 1 số, 1 ký tự đặc biệt. VD: Abc@1234</p>
        </div>
        <div className='p-4 text-lg bg-white text-left'>
            <div>
                <p className='font-medium'>Mật Khẩu Hiện Tại:</p>
                <Input.Password onChange={(e) => setOldPassword(e.target.value)} value={oldPassword} placeholder='Nhập mật khẩu hiện tại' className='py-2 text-lg'></Input.Password>
            </div>
            <div className='mt-5'>
                <p className='font-medium'>Mật Khẩu Mới:</p>
                <Input.Password onChange={(e) => setPassword(e.target.value)} className='py-2 text-lg' placeholder='Nhập mật khẩu hiện mới' value={password}></Input.Password>
                <p className='text-red-500 text-base'>{waring1}</p>
                <Input.Password onChange={(e) => setPassword2(e.target.value)} className='py-2 text-lg mt-5' placeholder='Nhập lại mật khẩu hiện mới' value={password2}></Input.Password>
                <p className='text-red-500 text-base'>{waring2}</p>
            </div>
            <div className='text-center'>
                <button onClick={handleUpdatePass} className='bg-sky-500 text-white py-2 w-3/5 mt-3 rounded-md'>Cập nhật</button>
            </div>
        </div>
        <ToastContainer />
    </div>
  )
}
