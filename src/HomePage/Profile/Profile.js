import React from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { useNavigate } from 'react-router';
import { Avatar } from '@mui/material';
import { useSelector } from 'react-redux';
import { Input } from 'antd';

export default function Profile() {
    let navigate= useNavigate();
    let user = useSelector((state) => state.UserSlice.user);
    let handleUpdateUser = () => {
        // Gọi api update user ở đây
    }
  return (
    <div id='profile' className='h-screen bg-gray-200'>
        <div>
            <button onClick={() => navigate(-1)} className='w-full text-left pl-4 text-white bg-gradient-to-r from-cyan-500 to-blue-500 text-2xl flex items-center py-2'><ArrowBackIosIcon className='mr-2'></ArrowBackIosIcon>  Thông Tin Cá Nhân</button>
        </div>
        <div className='flex p-4 items-center bg-white'>
            <div className='w-1/3 flex justify-center'>
                <Avatar style={{width:"6rem",height:"6rem"}} alt="Remy Sharp" src={user?.user_avatar} />
            </div>
            <div className='w-2/3 text-left text-lg'>
                <h2 className='text-sky-500 text-2xl font-semibold'>{user?.user_fullname}</h2>
                <p>Chức vụ: <span>{user?.user_chucVu}</span></p>
                <p>Mã NV: <span>{user?.user_maNV}</span></p>
            </div>
        </div>
        <div className='bg-white mt-3 p-4 text-left text-lg'>
            <div style={{borderBottom:"1px solid #01b4e2"}}>
                <p className='font-medium'>Tên Đăng Nhập</p>
                <p className='pb-2 pl-2'>{user?.user_username}</p>
            </div>
            <div style={{borderBottom:"1px solid #01b4e2"}}>
                <p className='font-medium'>Số Điện Thoại</p>
                <Input className='pb-2 border-none text-lg' type="text" defaultValue={user?.user_phone} />
            </div>
            <div style={{borderBottom:"1px solid #01b4e2"}}>
                <p className='font-medium'>Email</p>
                <Input className='pb-2 border-none text-lg' type="text" defaultValue={user?.user_email} />
            </div>
            <div style={{borderBottom:"1px solid #01b4e2"}}>
                <p className='font-medium'>Địa Chỉ</p>
                <Input className='pb-2 border-none text-lg' type="text" defaultValue={user?.user_address} />
            </div>
            <div className='text-center'>
                <button onClick={handleUpdateUser} className='bg-sky-500 text-white py-2 w-3/5 mt-3 rounded-md'>Cập nhật</button>
            </div>
        </div>
    </div>
  )
}
