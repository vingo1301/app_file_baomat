import React, { useState } from 'react'
import { useNavigate, useParams } from 'react-router'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import SearchIcon from '@mui/icons-material/Search';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import icon_folder from '../../isset/img/folder.jpg'
import icon_pdf from '../../isset/img/pdf.png'
import icon_word from '../../isset/img/word.png'
import icon_excel from '../../isset/img/excel.png'
import icon_file from '../../isset/img/file.png'
import MessageOutlinedIcon from '@mui/icons-material/MessageOutlined';
import moment from 'moment/moment';
import ChatButton from '../ChatButton';
import { Checkbox, FormControlLabel, Radio, RadioGroup } from '@mui/material';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import { getQuanTrong } from '../../Services/QuanTrongService/QuanTrongService';
import { useDispatch } from 'react-redux';
import { setFileId, setShowComment } from '../../Redux-toolkit/reducer/UserSlice';
import Comments from '../Comments/Comments';

export default function ChuY() {
    const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
    let navigate = useNavigate();
    let dispatch = useDispatch();
    let folderId = useParams().id;
    let [fileArray,setFileArray] = useState(getQuanTrong);
    let [fileArrayClone,setFileArrayClone] = useState(getQuanTrong);
    let [hideMenu,setHideMenu] = React.useState(true);

    let handleOpenComment = (id) => {
        dispatch(setShowComment(true));
        dispatch(setFileId(id));
    }
    // xử lý outside click
    const useOutsideAlerter = (ref) => {
        React.useEffect(() => {
          /**
           * Alert if clicked on outside of element
           */
          function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
              setHideMenu(true)
            }
          }
          // Bind the event listener
          document.addEventListener("mousedown", handleClickOutside);
          return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
          };
        }, [ref]);
      }
      const wrapperRef = React.useRef(null);
      useOutsideAlerter(wrapperRef);
    // 

    let handelSort = () => {
        // gọi api sắp xếp ở đây
    }

    let renderIconFile = (fileType) => {
        switch(fileType){
            case "folder": return <img src={icon_folder} className='w-full' alt="" />
            case "pdf": return <img src={icon_pdf} className='w-full' alt="" />
            case "word": return <img src={icon_word} className='w-full' alt="" />
            case "excel": return <img src={icon_excel} className='w-full' alt="" />
            default: return <img src={icon_file} className='w-full' alt="" />
        }
    }
    let changeSearch = (e) => {
        let array = [];
        if(e.target.value == "" || e.target.value == null){
            setFileArray(fileArrayClone);
            return;
        }
        fileArrayClone?.map((file) => {
            if(file.file_name.toLowerCase().includes(e.target.value.toLowerCase())){
                array.push(file);
            }
        });
        setFileArray(array);
    }
    let handleBackButton = () => {
        navigate(-1);
    }
    let handleLoadFile = (type) => {
        if(type == "folder"){
             // chỗ này gọi api lấy file theo thư mục con
        }else if(type == "pdf"){
            // mở trình xem pdf
            navigate("/pdfViewer")
        }else{
            // tải file về
        }
       
    }
    let renderFile = () => {
        return fileArray?.map((file,index) => {
            return <div style={{borderBottom:"1px solid #01b4e2"}} className='flex items-center p-3'>
                <div className='sm:w-1/12 w-1/6'>
                    {renderIconFile(file?.file_type)}
                </div>
                <div onClick={() => handleLoadFile(file?.file_type)} className=' text-left ml-2 cursor-pointer'>
                    <h2 className='text-xl font-semibold'>{file.file_name}</h2>
                    <p className='text-sm text-gray-500'>{moment(file?.file_lastupdate).format("DD/MM/YYYY")}- Upload lần cuối bởi: {file?.user?.user_name}</p>
                </div>
                <div className='p-2 ml-auto'>
                    {
                        file?.file_type == "folder"?"":<button onClick={() => handleOpenComment(file?.file_id)}><MessageOutlinedIcon fontSize='large'></MessageOutlinedIcon></button>
                    }
                </div>
            </div>
        })
    }
  return (
    <div id='chuY' className='h-screen w-full relative'>
        <div className='header flex p-2 bg-gradient-to-r from-cyan-500 to-blue-500'>
            <div className='w-1/2 text-left'>
                <button onClick={handleBackButton}><p className='text-white text-xl md:text-2xl'><ArrowBackIosIcon/>Cần Chú Ý</p></button>
            </div>
            <div className='w-1/2 flex items-center justify-end'>
                <div style={{maxWidth:"70%"}} className='flex h-full items-center'>
                    <input onChange={changeSearch} style={{width:"80%"}} className='h-full focus:outline-none pl-2' type="text" />
                    <button disabled style={{borderLeft:"1px solid #01b4e2"}} className='bg-white text-blue-500 h-full px-1'>
                        <SearchIcon></SearchIcon>
                    </button>
                </div>
                <div style={{maxWidth:"30%"}}>
                    <button onClick={() => {setHideMenu(false)}} className='text-white ml-5'><FilterAltIcon fontSize='large'></FilterAltIcon></button>
                </div>
            </div>
        </div>
        <div className=''>
            {renderFile()}
        </div>
        <div ref={wrapperRef} style={{maxWidth:"80%",transition:"all 0.5",borderRight:"1px solid #d5d5d5"}} className={`phanLoai h-screen bg-white absolute z-10 top-0 left-0 text-left overflow-hidden ${hideMenu&&"w-0"}`}>
            <div className='p-4 bg-gradient-to-r from-cyan-500 to-blue-500'>
                <button onClick={() => {setHideMenu(true)}}><p className='text-white text-2xl'><ArrowBackIosIcon/>Phân Loại</p></button>
            </div>
            <div className=' text-lg font-medium'>
                <p className='bg-gray-200 p-2'>Phân Loại Theo</p>
                <div style={{borderBottom:"1px solid #01b4e2"}} className='p-1 flex justify-between'>
                    <div className='flex items-center'>
                        <img className='w-10 mr-2' src={icon_file} alt="" />
                        <p>Tất Cả</p>
                    </div>
                    <Checkbox {...label} />
                </div>
                <div style={{borderBottom:"1px solid #01b4e2"}} className='p-1 flex justify-between'>
                    <div className='flex items-center'>
                        <img className='w-10 mr-2' src={icon_word} alt="" />
                        <p>Word</p>
                    </div>
                    <Checkbox {...label} />
                </div>
                <div style={{borderBottom:"1px solid #01b4e2"}} className='p-1 flex justify-between'>
                    <div className='flex items-center'>
                        <img className='w-10 mr-2' src={icon_pdf} alt="" />
                        <p>Pdf</p>
                    </div>
                    <Checkbox {...label} />
                </div>
                <div style={{borderBottom:"1px solid #01b4e2"}} className='p-1 flex justify-between'>
                    <div className='flex items-center'>
                        <img className='w-10 mr-2' src={icon_excel} alt="" />
                        <p>Excel</p>
                    </div>
                    <Checkbox {...label} />
                </div>
            </div>
            <div className='text-lg font-medium'>
                <p className='bg-gray-200 p-2'>Sắp xếp theo</p>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label" defaultValue="1" name="radio-buttons-group">
                    <div className='flex justify-between items-center'>
                        <p className='pl-2'><AccessTimeIcon></AccessTimeIcon><span className='ml-2'>Ngày Upload Gần Nhất</span></p>
                        <FormControlLabel style={{margin:0}} value="0" control={<Radio />} />
                    </div>
                    <div className='flex justify-between items-center'>
                    <p className='pl-2'><MessageOutlinedIcon></MessageOutlinedIcon><span className='ml-2'>Nhận Xét Mới Nhất</span></p>
                        <FormControlLabel style={{margin:0}} value="1" control={<Radio />} />
                    </div>
                </RadioGroup>
                <button onClick={handelSort} className='w-full bg-blue-500 text-white mt-3 py-2'>Áp Dụng</button>
            </div>
        </div>
        <ChatButton></ChatButton>
        <Comments></Comments>
    </div>
  )
}
