import React, { useState } from 'react'
import {getThuMuc} from '../../Services/ThuMucService/ThuMucService'
import icon_folder from '../../isset/img/folder.jpg'
import { useNavigate } from 'react-router';



export default function ThuMuc() {
  let [thuMucArray,setThuMucArray] = useState(getThuMuc);
  let navigate = useNavigate();

  let getFolderItem = (id) => {
        navigate("/thumuc/"+ id);
  }

  let renderThuMuc = () => {
    return thuMucArray.map((thucMuc,index) => {
      return <div className='m-4'>
        <div onClick={() => getFolderItem(thucMuc.thumuc_id)} style={{width:"4rem"}} className='mx-auto cursor-pointer'>
          <img src={icon_folder} alt="" />
        </div>
        <p className='mt-2 font-semibold'>{thucMuc.thumuc_name}</p>
      </div>
    })
  }
  return (
    <div id='thuMuc' className='relative'>
      <div style={{width:"15px",height:"15px",left:"15%"}} className='square rotate-45 absolute -top-2 bg-white'>
      </div>
      <div className='grid grid-cols-2 md:grid-cols-3'>
        {renderThuMuc()}
      </div>
      
    </div>
  )
}
