import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import InputBase from '@mui/material/InputBase';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import img_chungNhan from '../isset/img/1.png';
import img_thuMuc from '../isset/img/2.png'
import img_kiemDuyet from '../isset/img/3.png'
import ThuMuc from './ThuMuc/ThuMuc'
import KiemDuyet from './KiemDuyet/KiemDuyet'
import ChungNhan from './ChungNhan/ChungNhan'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import HomeIcon from '@mui/icons-material/Home';
import { Avatar } from 'antd';
import { useSelector } from 'react-redux';
import TaskAltIcon from '@mui/icons-material/TaskAlt';
import PriorityHighIcon from '@mui/icons-material/PriorityHigh';
import NotificationsIcon from '@mui/icons-material/Notifications';
import LockIcon from '@mui/icons-material/Lock';
import LocalPoliceIcon from '@mui/icons-material/LocalPolice';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import ChatButton from './ChatButton';
import { useNavigate } from 'react-router';
import Comments from './Comments/Comments';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '14ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));


export default function HomePage() {
  let img_array = [{img:img_thuMuc,name:"Thư Mục"},{img:img_kiemDuyet,name:"Kiểm Duyệt"},{img:img_chungNhan,name:"Chứng Nhận"}];
  let [tab,setTab] = React.useState(1);
  let [hideMenu,setHideMenu] = React.useState(true);
  let user = useSelector((state) => state.UserSlice.user);
  let navigate= useNavigate();
  const useOutsideAlerter = (ref) => {
    React.useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setHideMenu(true)
        }
      }
      // Bind the event listener
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }
  const wrapperRef = React.useRef(null);
  useOutsideAlerter(wrapperRef);
  let renderTab = () => {
    switch (tab) {
      case 1: return <ThuMuc></ThuMuc>
      case 2: return <KiemDuyet></KiemDuyet>
      case 3: return <ChungNhan></ChungNhan>
      default: return <ThuMuc></ThuMuc>
    }
  }
  return (
    <div id='homePage' className='h-screen relative'>
      <div className='header bg-gradient-to-r from-cyan-500 to-blue-500'>
        {/* thanh header */}
        <Box sx={{ flexGrow: 1 }}>
          <AppBar position="static">
            <Toolbar>
              <IconButton onClick={()=>{setHideMenu(false)}}
                size="large"
                edge="start"
                color="inherit"
                aria-label="open drawer"
                sx={{ mr: 2 }}
              >
                <MenuIcon />
              </IconButton>
              <Typography
                variant="h6"
                noWrap
                component="div"
                sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
              >
                {/* MUI */}
              </Typography>
              <Search>
                <SearchIconWrapper>
                  <SearchIcon />
                </SearchIconWrapper>
                <StyledInputBase
                  placeholder="Tìm Kiếm File..."
                  inputProps={{ 'aria-label': 'search' }}
                />
              </Search>
            </Toolbar>
          </AppBar>
        </Box>
        {/* Thanh tab */}
        <div className='grid grid-cols-3 bg-gradient-to-r from-cyan-500 to-blue-500 py-3'>
          {
            img_array.map((img,index) => {
              return <div className='h-full'>
              <div style={{width:"3rem"}} onClick={()=>{setTab(index+1)}} className='bg-white p-3 mx-auto rounded-full cursor-pointer'>
                <img src={img.img} alt="" />
              </div>
              <p className='text-white mt-2 active'><span>{img.name}</span></p>
            </div>
            })
          }
        </div>
      </div>
      {/* Tab nội dung */}
      <div className=''>
          {renderTab()}
      </div>
      {/* Thanh menu user */}
      <div ref={wrapperRef} style={{maxWidth:"80%",transition:"all 0.5",borderRight:"1px solid #d5d5d5"}} className={`h-full bg-white absolute top-0 left-0 text-left ${hideMenu&&"w-0"} overflow-hidden`}>
          <div className='p-4 bg-gradient-to-r from-cyan-500 to-blue-500'>
            <button onClick={() => {setHideMenu(true)}}><p className='text-white text-2xl'><ArrowBackIosIcon/>Menu</p></button>
          </div>
          <div onClick={() => navigate("/profile")} style={{borderBottom:"1px solid #d5d5d5"}} className='flex p-3 cursor-pointer'>
              <div style={{width:"4rem",height:"4rem"}} className=''>
                <Avatar className='w-full h-full' alt="Remy Sharp" src={user?.user_avatar} />
              </div>
              <div className='ml-2 text-left self-center'>
                <h2 className='text-sky-500 text-lg font-semibold'>{user?.user_fullname}</h2>
                <p className='text-gray-500'>{user?.user_chucVu}</p>
              </div>
          </div>
          <div className=''>
            <button onClick={() => navigate("/")} className='w-full text-left pl-4 text-white bg-gradient-to-r from-cyan-500 to-blue-500 text-xl flex items-center py-1'><HomeIcon className='mr-2'></HomeIcon>  Trang Chủ</button>
            <button onClick={() => navigate("/tatCa")} className='w-full text-left pl-4 bg-green-500 text-white text-xl flex items-center py-1'><TaskAltIcon className='mr-2'></TaskAltIcon> Tất cả</button>
            <button onClick={() => navigate("/quanTrong")} className='w-full text-left pl-4 bg-red-500 text-white text-xl flex items-center py-1'><PriorityHighIcon className='mr-2'></PriorityHighIcon> Quan Trọng</button>
            <button onClick={() => navigate("/chuY")} className='w-full text-left pl-4 bg-yellow-500 text-white text-xl flex items-center py-1'><NotificationsIcon className='mr-2'></NotificationsIcon>Cần Chú Ý</button>
            <button onClick={() => navigate("/changePassword")} className='w-full text-left pl-4 text- flex items-center py-1'><LockIcon className='mr-2'></LockIcon>Đổi Mật Khẩu</button>
            <button className='w-full text-left pl-4 text-lg flex items-center py-1'><LocalPoliceIcon className='mr-2'></LocalPoliceIcon>Chính Sách Bảo Mật</button>
            <button className='w-full text-left pl-4 text-lg flex items-center py-1'><ErrorOutlineIcon className='mr-2'></ErrorOutlineIcon>Phiên Bản 1.1</button>
            <button className='w-full bg-red-500 text-white text-xl flex justify-center items-center py-1 absolute bottom-0 left-0'><PowerSettingsNewIcon className='mr-2'></PowerSettingsNewIcon>Đăng Xuất</button>
          </div>
      </div>
      <ChatButton></ChatButton>
      <Comments></Comments>
    </div>
  )
}
