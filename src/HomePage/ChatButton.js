import React from 'react'
import icon_chat from '../isset/img/chat.png'
import "./HomePage.css"
import { CircleMenu, CircleMenuItem } from "react-circular-menu";
import HomeIcon from '@mui/icons-material/Home';
import ChatIcon from '@mui/icons-material/Chat';
import AddCommentIcon from '@mui/icons-material/AddComment';
import GroupIcon from '@mui/icons-material/Group';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import { useNavigate } from 'react-router';

export default function ChatButton() {
  let navigate= useNavigate();
  return (
      <div id='chatButton' style={{width:"3rem",height:"3rem",right:"12%",bottom:"13%"}} className='fixed z-20'>
          <CircleMenu
              startAngle={90}
              rotationAngle={270}
              itemSize={2}
              radius={4}
              rotationAngleInclusive={false}
            >
              <CircleMenuItem
                onClick={() => navigate("/taoNhom")}
                tooltip="Tạo Nhóm Mới"
                tooltipPlacement="left"
              >
                <GroupAddIcon className='text-white'></GroupAddIcon>
              </CircleMenuItem>
              <CircleMenuItem
                onClick={() => navigate("/chatNhom")}
                tooltip="Chat Nhóm"
                tooltipPlacement="left"
              >
                <GroupIcon className='text-white'></GroupIcon>
              </CircleMenuItem>
              <CircleMenuItem onClick={() => navigate("/addChat")} tooltip="Trò Chuyện Mới" tooltipPlacement="left">
                <AddCommentIcon className='text-white'></AddCommentIcon>
              </CircleMenuItem>
              <CircleMenuItem onClick={() => navigate("/chat")} tooltip="Trò Chuyện" tooltipPlacement="left">
                <ChatIcon className='text-white'></ChatIcon>
              </CircleMenuItem>
              <CircleMenuItem onClick={() => navigate("/")} tooltip="Trang Chủ" tooltipPlacement="left">
                <HomeIcon className='text-white'></HomeIcon>
              </CircleMenuItem>
          </CircleMenu>
      </div>
  )
}
