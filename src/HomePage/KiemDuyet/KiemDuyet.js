import React, { useState } from 'react'
import icon_pdf from '../../isset/img/pdf.png'
import icon_word from '../../isset/img/word.png'
import icon_excel from '../../isset/img/excel.png'
import icon_file from '../../isset/img/file.png'
import { getKiemDuyet } from '../../Services/KiemDuyetService/KiemDuyetService'
import moment from 'moment/moment'
import { useNavigate } from 'react-router'

export default function KiemDuyet() {
  let [fileArray,setFileArray] = useState(getKiemDuyet);
  let navigate = useNavigate();

  let handleDuyetFile = (id) => {
    // gọi api duyệt file ở đây
  }
  let handleLoadFile = (type) => {
    if(type == "folder"){
         // chỗ này gọi api lấy file theo thư mục con
    }else if(type == "pdf"){
        // mở trình xem pdf
        navigate("/pdfViewer")
    }else{
        // tải file về
    }
  }
  let renderIconFile = (fileType) => {
    switch(fileType){
        case "pdf": return <img src={icon_pdf} className='w-full' alt="" />
        case "word": return <img src={icon_word} className='w-full' alt="" />
        case "excel": return <img src={icon_excel} className='w-full' alt="" />
        default: return <img src={icon_file} className='w-full' alt="" />
    }
  }
  let renderFile = () => {
    return fileArray?.map((file,index) => {
      return <div style={{borderBottom:"1px solid #01b4e2"}} className='flex items-center p-3'>
          <div className='sm:w-1/12 w-1/6'>
              {renderIconFile(file?.file_type)}
          </div>
          <div onClick={() => handleLoadFile(file?.file_type)} className=' text-left ml-2 cursor-pointer'>
              <h2 className='text-xl font-semibold'>{file.file_name}</h2>
              <p className='text-sm text-gray-500'>{moment(file?.file_lastupdate).format("DD/MM/YYYY")}- Upload lần cuối bởi: {file?.user?.user_name}</p>
          </div>
          <div className='p-2 ml-auto'>
              {
                  <button onClick={() => handleDuyetFile(file.file_id)} className='bg-sky-500 text-white px-3 py-2 rounded-xl'>Duyệt</button>
              }
          </div>
      </div>
    })
  }
  return (
    <div id='kiemDuyet' className='relative'>
      <div style={{width:"15px",height:"15px",left:"48%",top:"-1.5%"}} className='square rotate-45 absolute bg-white'>
      </div>
      <div>
          {renderFile()}
      </div>
    </div>
  )
}
