export const getChatList = [
    {
        chat_id: 1,
        chat_name: "Nhóm Hành Chính",
        user: [
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            },
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            },
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            },
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            },
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            }
        ],
        message: {
            message_last: "@abc1: Dạ",
            message_time: "1 Giờ"
        }
    },
    {
        chat_id: 2,
        chat_name: "Ban Giám Đốc",
        user: [
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            },
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            },
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            },
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            }
        ],
        message: {
            message_last: "Dạ",
            message_time: "1 Giờ"
        }
    },
    {
        chat_id: 1,
        chat_name: "Nhóm Tự Kỷ",
        user: [
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            },
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            },
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            }
        ],
        message: {
            message_last: "Dạ",
            message_time: "1 Giờ"
        }
    },
    {
        chat_id: 1,
        chat_name: "Nhóm Kín",
        user: [
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            },
            {
                user_id: 1,
                user_username: "abcd1",
                user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png"
            }
        ],
        message: {
            message_last: "Dạ",
            message_time: "1 Giờ"
        }
    }
]