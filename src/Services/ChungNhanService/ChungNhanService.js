export const getChungNhan = [
    {
        file_id: 2,
        file_name: "Báo cáo công nợ 09/2023",
        file_type:"excel",
        user:{
            user_name: "Người dùng 1"
        },
        file_lastupdate: "2023/09/26"
    },
    {
        file_id: 3,
        file_name: "Điều khoản sử dụng",
        file_type:"word",
        user:{
            user_name: "Người dùng 2"
        },
        file_lastupdate: "2023/09/26"
    },
    {
        file_id: 4,
        file_name: "Hợp đồng lao động",
        file_type:"pdf",
        user:{
            user_name: "Người dùng 3"
        },
        file_lastupdate: "2023/09/26"
    },
    {
        file_id: 4,
        file_name: "Hợp đồng lao động",
        file_type:"file",
        user:{
            user_name: "Người dùng 4"
        },
        file_lastupdate: "2023/09/26"
    }
]
