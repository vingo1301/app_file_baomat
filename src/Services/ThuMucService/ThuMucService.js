export const getThuMuc = [
    {
        thumuc_id: 1,
        thumuc_name: "Lãnh Đạo "
    },
    {
        thumuc_id: 2,
        thumuc_name: "Hành Chính "
    },
    {
        thumuc_id: 3,
        thumuc_name: "Kế Toán "
    },
    {
        thumuc_id: 4,
        thumuc_name: "Nhân Sự "
    },
    {
        thumuc_id: 5,
        thumuc_name: "Bán Hàng "
    },
    {
        thumuc_id: 6,
        thumuc_name: "Sản Xuất "
    }
]
// lấy danh sách thư mục con, file trong thư mục
export const getFile = {
    thumuc_id: 1,
    thumuc_name: "Ban Lãnh Đạo",
    children:[
        {
            file_id: 1,
            file_name: "09/2023",
            file_type:"folder",
            file_lastupdate: "2023/09/26"
        },
        {
            file_id: 2,
            file_name: "Báo cáo công nợ 09/2023",
            file_type:"excel",
            user:{
                user_name: "Người dùng 1"
            },
            file_lastupdate: "2023/09/26"
        },
        {
            file_id: 3,
            file_name: "Điều khoản sử dụng",
            file_type:"word",
            user:{
                user_name: "Người dùng 2"
            },
            file_lastupdate: "2023/09/26"
        },
        {
            file_id: 4,
            file_name: "Hợp đồng lao động",
            file_type:"pdf",
            user:{
                user_name: "Người dùng 3"
            },
            file_lastupdate: "2023/09/26"
        },
        {
            file_id: 4,
            file_name: "Hợp đồng lao động",
            file_type:"file",
            user:{
                user_name: "Người dùng 4"
            },
            file_lastupdate: "2023/09/26"
        }
    ]
}