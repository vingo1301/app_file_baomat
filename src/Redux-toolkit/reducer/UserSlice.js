import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  user: {
    user_id: 1,
    user_fullname: "Ngọc Thương",
    user_chucVu: "Nhân Viên",
    user_avatar: "https://s2.coinmarketcap.com/static/img/coins/200x200/24988.png",
    user_address: "200 Đường số 9, phường Tân Phú, Quận 7",
    user_phone: "0770770777",
    user_maNV: "123456",
    user_username: "navitek.ngocthuong",
    user_email: "ngocthuong@gmail.com"
  },
  showComment: false,
  fileId: 0
}

export const UserSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setShowComment: (state,action) => {
      state.showComment = action.payload;
    },
    setFileId: (state,action) => {
      state.fileId = action.payload
    }
  },
})

// Action creators are generated for each case reducer function
export const {setShowComment,setFileId} = UserSlice.actions

export default UserSlice.reducer