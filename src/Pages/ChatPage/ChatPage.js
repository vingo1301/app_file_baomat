import React from 'react'
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import "./ChatPage.css"
import ChatButton from '../../HomePage/ChatButton';
import { useNavigate } from 'react-router';
import { getChatList } from '../../Services/ChatService/ChatService';

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  }));
  
  const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }));
  
  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        width: '14ch',
        '&:focus': {
          width: '20ch',
        },
      },
    },
  }));
  let renderChat = () => {
    return getChatList?.map((chat,index) => {
        return <div style={{borderBottom:"1px solid #01b4e2"}} className='flex justify-between py-3 hover:bg-gray-200 cursor-pointer'>
            <div style={{width:"4.5rem"}}>
                <img style={{width:"100%",borderRadius:"100%"}} src={chat?.user?.user_avatar} alt="" />
            </div>
            <div className='w-full text-left ml-3'>
                <p className='text-lg font-medium'>@{chat?.user?.user_username}</p>
                <p>{chat?.message?.message_last}</p>
            </div>
            <div style={{width:"4rem"}}>
                <p>{chat?.message?.message_time}</p>
                <div style={{width:"2rem", height:"2rem"}}><p style={{fontSize:"1rem",lineHeight:"2rem"}} className='w-full h-full align-middle bg-red-500 rounded-full text-white'>1</p></div>
            </div>
        </div>
    })
  }
export default function ChatPage() {
    let navigate = useNavigate();
  return (
    <div id='chatPage' className='h-screen w-full flex flex-col'>
        <div className='header'>
            <div className='header p-2 bg-gradient-to-r from-cyan-500 to-blue-500'>
                <div className='max-w-md text-left text-white'>
                    <Search>
                        <SearchIconWrapper>
                            <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Tìm Kiếm..."
                            inputProps={{ 'aria-label': 'search' }}
                    />
                    </Search>
                </div>
            </div>
        </div>
        <div style={{borderBottom:"1px solid #d5d5d5"}} className='text-left p-3 text-lg font-medium'>
            <button onClick={() => navigate("/chat")} className='active'>Cá Nhân</button>
            <button onClick={() => navigate("/chatNhom")} className='ml-5'>Nhóm</button>
        </div>
        <div className='h-full overflow-y-scroll p-2'>
                {renderChat()}
        </div>
        <ChatButton></ChatButton>
    </div>
  )
}
