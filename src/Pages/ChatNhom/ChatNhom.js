import React from 'react'
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import "../ChatPage/ChatPage.css"
import ChatButton from '../../HomePage/ChatButton';
import { useNavigate } from 'react-router';
import { getChatList } from '../../Services/ChatNhomService/ChatNhomService';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '14ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));
export default function ChatNhom() {
  let navigate = useNavigate();
  let renderAvatar = (users) => {
      let length = users.length;
      if(length <= 4){
        return users?.map((user,index) => {
          return <img className='rounded-full' src={user?.user_avatar} alt="" />
        })
      }else{
        return users?.map((user,index) => {
          if(index <= 2){
            return <img className='rounded-full' src={user?.user_avatar} alt="" />
          }else if(index ==3){
            return <p className='bg-gray-500 text-white rounded-full'>{length}</p>
          }else{
            return;
          }
        })
      }
  }
  let renderChatList = () => {
    return getChatList?.map((chat,index) => {
      return <div style={{borderBottom:"1px solid #01b4e2"}} className='flex justify-between py-3 hover:bg-gray-200 cursor-pointer'>
          <div style={{width:"4.5rem"}}>
            <div className='grid grid-cols-2 gap-1'>
              {renderAvatar(chat?.user)}
            </div>
          </div>
          <div className='w-full text-left ml-3'>
                <p className='text-lg font-medium'>{chat?.chat_name}</p>
                <p>{chat?.message?.message_last}</p>
            </div>
          <div style={{width:"4rem"}}>
                <p>{chat?.message?.message_time}</p>
                <div style={{width:"2rem", height:"2rem"}}><p style={{fontSize:"1rem",lineHeight:"2rem"}} className='w-full h-full align-middle bg-red-500 rounded-full text-white'>1</p></div>
            </div>
      </div>
    })
  }
  return (
    <div id='chatPage' className='h-screen w-full flex flex-col'>
        <div className='header'>
            <div className='header p-2 bg-gradient-to-r from-cyan-500 to-blue-500'>
                <div className='max-w-md text-left text-white'>
                    <Search>
                        <SearchIconWrapper>
                            <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Tìm Kiếm..."
                            inputProps={{ 'aria-label': 'search' }}
                    />
                    </Search>
                </div>
            </div>
        </div>
        <div style={{borderBottom:"1px solid #d5d5d5"}} className='text-left p-3 text-lg font-medium'>
            <button onClick={() => navigate("/chat")} className=''>Cá Nhân</button>
            <button onClick={() => navigate("/chatNhom")} className='ml-5 active'>Nhóm</button>
        </div>
        <div className='h-full overflow-y-scroll p-2'>
                {renderChatList()}
        </div>
        <ChatButton></ChatButton>
    </div>
  )
}
