import React, { useState } from 'react'
import ChatButton from '../../HomePage/ChatButton'
import { useNavigate } from 'react-router'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { getUsers } from '../../Services/ChatService/ChatService';
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import { Box, Checkbox, TextField } from '@mui/material';
import { Tooltip } from 'antd';
import SendIcon from '@mui/icons-material/Send';
import ModeEditIcon from '@mui/icons-material/ModeEdit';


const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  }));
  
  const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }));
  
  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        width: '14ch',
        '&:focus': {
          width: '20ch',
        },
      },
    },
  }));
  const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
export default function CreateGroup() {
    let navigate = useNavigate();
    let [userList,setUserList] = useState(getUsers);
    let [userListClone,setUserListClone] = useState(getUsers);
    let [selectList,setSelectList] = useState([]);

    let check = (user) => {
        let ind = selectList.findIndex((item) => {
            return item == user;
        });
        // console.log(ind);
        if(ind == -1){
            return false;
        }else{
            return true;
        }
    }
    let changeSelect = (e,user) => {
        let array = [...selectList];
        let checked = e.target.checked;
        if(checked){
            array.push(user);
        }else{
            let ind = selectList.findIndex((item) => {
                return item == user;
            });
            array.splice(ind,1)
        }
        setSelectList(array);
    }
    let handleCreateChat = () => {
        // gọi api tạo chat ở đây sau đó chuyển hướng
    }
    let changeSearch = (e) => {
        let array = [];
        if(e.target.value == "" || e.target.value == null){
            setUserList(userListClone);
            return;
        }
        userListClone?.map((user) => {
            if(user.user_username.toLowerCase().includes(e.target.value.toLowerCase())){
                array.push(user);
            }
        });
        setUserList(array);
    }
    let renderSelectList = () => {
        return selectList?.map((user,index) => {
            return <div style={{width:"3rem"}} className='ml-2'>
                <Tooltip title={user?.user_username}>
                    <img className='rounded-full w-full' src={user?.user_avatar} alt="" />
                </Tooltip>
            </div>
        })
    }
    let renderUserList = () => {
        return userList?.map((user,index) => {
            return <div className='flex mx-auto w-2/3 md:w-1/3 items-center p-2 hover:bg-gray-100'>
                <div>
                    <Checkbox checked={check(user)} onChange={(e) => changeSelect(e,user)} {...label} />
                </div>
                <div style={{width:"3rem"}} className=''>
                    <img className='rounded-full w-full' src={user?.user_avatar} alt="" />
                </div>
                <div>
                    <p className='ml-3 text-lg'>@{user?.user_username}</p>
                </div>
            </div>
        })
    }
  return (
    <div id='createGroup' className='h-screen w-full flex flex-col'>
        <div className='header'>
            <div className='header p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-left'>
                <div className='md:w-1/3 w-2/3'>
                    <button onClick={() => navigate(-1)}><p className='text-white text-2xl'><ArrowBackIosIcon/>Trò Chuyện Mới</p></button>
                </div>
            </div>
        </div>
        <div className=' bg-gray-100 flex flex-col'>
                <div className='flex justify-center mt-3'>
                    <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                        <ModeEditIcon sx={{ color: 'action.active', mr: 1, my: 0.5 }} />
                        <TextField id="input-with-sx" label="Đặt Tên Nhóm" variant="standard" />
                    </Box>
                </div>
                <h2 className='m-3 text-xl font-semibold'>Danh Sách Nhân Viên</h2>
                <div className='text-left lg:w-1/3 w-1/2 mx-auto text-white mb-3'>
                    <Search onChange={changeSearch} style={{background:"white",color:"black",border:"1px solid #01b4e2"}}>
                        <SearchIconWrapper>
                            <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Tìm Kiếm..."
                            inputProps={{ 'aria-label': 'search' }}
                    />
                    </Search>
                </div>
                <div className='bg-white h-full overflow-y-scroll'>
                    {renderUserList()}
                </div>
                <div className='w-4/5 mx-auto flex p-3'>
                    <div className='flex justify-center w-full overflow-x-scroll'>
                        {renderSelectList()}
                    </div>
                    <button onClick={handleCreateChat} className='text-blue-500 ml-3'><SendIcon fontSize='large'></SendIcon></button>
                </div>
        </div>
        <ChatButton></ChatButton>
    </div>
  )
}
