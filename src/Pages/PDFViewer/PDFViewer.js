import React from 'react'
import pdf_test from '../../isset/files/13-[tai_lieu]-socket-io.pdf'
import { Worker, Viewer } from "@react-pdf-viewer/core";
import { defaultLayoutPlugin } from "@react-pdf-viewer/default-layout";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';

import "@react-pdf-viewer/core/lib/styles/index.css";
import "@react-pdf-viewer/default-layout/lib/styles/index.css";
import { useNavigate } from 'react-router';
import Comments from '../../HomePage/Comments/Comments';
import MessageOutlinedIcon from '@mui/icons-material/MessageOutlined';
import { useDispatch } from 'react-redux';
import { setFileId, setShowComment } from '../../Redux-toolkit/reducer/UserSlice';


export default function PDFViewer() {
    let navigate = useNavigate();
    let dispatch = useDispatch();
    let handleOpenComment = (id) => {
      dispatch(setShowComment(true));
      dispatch(setFileId(id));
  }
    const renderToolbar = (Toolbar) => (
        <Toolbar>
          {(slots) => {
            const {
              ShowSearchPopover,
              CurrentPageInput,
              Download,
              GoToNextPage,
              GoToPreviousPage,
              NumberOfPages,
              Zoom,
              ZoomIn,
              ZoomOut,
              EnterFullScreen,
              Print,
              ShowProperties
            } = slots;
            return (
              <div style={{  fontFamily: "Helvetica"}} className='grid grid-cols-8 sm:grid-cols-11 w-full items-center'>
                <div style={{ padding: "0px 2px" }}>
                          <ShowSearchPopover />
                </div>
                <div className='col-span-3 flex items-center'>
                    <div style={{ padding: "0px 2px" }}>
                    <GoToPreviousPage />
                    </div>
                    <div style={{ padding: "0px 2px", display: "flex", alignItems: "center" }}>
                    <div style={{width:"40px"}}><CurrentPageInput /></div> / <NumberOfPages />
                    </div>
                    <div style={{ padding: "0px 2px" }}>
                    <GoToNextPage />
                    </div>
                </div>
                <div className='col-span-3 flex items-center'>
                    <div style={{ padding: "0px 2px", marginLeft: "auto" }}>
                    <ZoomOut />
                    </div>
                    <div style={{ padding: "0px 2px" }}>
                    <Zoom />
                    </div>
                    <div style={{ padding: "0px 2px" }}>
                    <ZoomIn />
                    </div>
                </div>
                <div style={{ padding: "0px 2px", marginLeft: "auto" }}>
                    <EnterFullScreen />
                </div>
                <div style={{ padding: "0px 2px" }}>
                    <Print />
                </div>
                <div style={{ padding: "0px 2px", marginLeft: "auto" }}>
                  <Download />
                </div>
                <div style={{ padding: "0px 2px" }}>
                    <ShowProperties />
                </div>
              </div>
            );
          }}
        </Toolbar>
      );
    
      const defaultLayoutPluginInstance = defaultLayoutPlugin({
        sidebarTabs: (defaultTabs) => [],
        renderToolbar
      });
  return (
    <div className='w-100% h-screen'>
        <div className='p-3 bg-gradient-to-r from-cyan-500 to-blue-500 text-left text-white flex justify-between items-center'>
            <button onClick={() => navigate(-1)}><p className='text-2xl'><ArrowBackIosIcon/>Tên File</p></button>
            <button onClick={() => handleOpenComment(1)}><MessageOutlinedIcon></MessageOutlinedIcon></button>
        </div>
        <Worker workerUrl="https://unpkg.com/pdfjs-dist@3.4.120/build/pdf.worker.min.js">
            <div style={{height:"90vh"}}>
                <Viewer fileUrl={pdf_test}
                    plugins={[defaultLayoutPluginInstance]} />
            </div>
        </Worker>
        <Comments></Comments>
    </div>
  )
}
