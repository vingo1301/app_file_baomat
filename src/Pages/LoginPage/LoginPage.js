import { Checkbox, Input } from 'antd'
import React, { useState } from 'react'
import { useNavigate } from 'react-router';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import logo from '../../isset/img/weos_preview.png'

export default function LoginPage() {
    let navigate = useNavigate();
    let [password,setPassword] = useState("");
    let [username,setUsername] = useState("");

    let changeUsername = (e) => {
        setUsername(e.target.value);
    }
    let changePassword = (e) => {
        setPassword(e.target.value);
    }
    let handleLogin = () => {
        if(username == ""){
            toast.error("Vui lòng nhập tài khoản!",{
                position: toast.POSITION.TOP_CENTER
            }); return;
        }
        if(password == ""){
            toast.error("Vui lòng nhập mật khẩu!",{
                position: toast.POSITION.TOP_CENTER
            }); return;
        };
        // Gọi API Đăng nhập ở đây
        navigate("/")

    }
  return (
    <div id='loginPage' className='h-screen bg-gray-200'>
        <div className='h-2/5'>
            <img className='w-60 mx-auto' src={logo} alt="" />
        </div>
        <div style={{borderTopLeftRadius:"30px",borderTopRightRadius:"30px",width:"100%"}} className='h-3/5 bg-gradient-to-r from-cyan-500 to-blue-500 relative'>
            <div className='loginForm bg-white py-7 rounded-2xl px-5 absolute left-1/2 top-0 -translate-x-1/2 -translate-y-12 w-10/12'>
                    <h2 className='text-2xl font-bold'>Đăng Nhập</h2>
                    <Input placeholder='Mã Công Ty.Tên Đăng Nhập' onChange={changeUsername} className='mt-7 py-2'></Input>
                    <Input.Password onChange={changePassword} className='mt-7 py-2'></Input.Password>
                    <Checkbox className='mt-5'><i>Ghi nhớ tài khoản</i></Checkbox>
                    <div className='mt-7'>
                        <button onClick={handleLogin} className='bg-blue-500 text-white font-semibold text-lg px-3 py-2 rounded-lg'>Đăng Nhập</button>
                    </div>
                    
            </div>
        </div>
        <ToastContainer />
    </div>
  )
}
