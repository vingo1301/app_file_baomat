
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from '../src/HomePage/HomePage'
import HOC from '../src/HOC/HOC'
import LoginPage from './Pages/LoginPage/LoginPage';
import ThuMucCon from './HomePage/ThuMuc/ThuMucCon/ThuMucCon';
import Profile from './HomePage/Profile/Profile';
import DoiMatKhau from './HomePage/Profile/DoiMatKhau';
import QuanTrong from './HomePage/QuanTrong/QuanTrong';
import ChuY from './HomePage/ChuY/ChuY';
import TatCa from './HomePage/TatCa/TatCa';
import PDFViewer from './Pages/PDFViewer/PDFViewer';
import ChatPage from './Pages/ChatPage/ChatPage';
import ChatNhom from './Pages/ChatNhom/ChatNhom';
import AddChat from './Pages/ChatPage/AddChat';
import CreateGroup from './Pages/ChatNhom/CreateGroup';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<HOC Component={HomePage}></HOC>}></Route>
          <Route path='/login' element={<HOC Component={LoginPage}></HOC>}></Route>
          <Route path='/thumuc/:id' element={<HOC Component={ThuMucCon}></HOC>}></Route>
          <Route path='/profile' element={<HOC Component={Profile}></HOC>}></Route>
          <Route path='/changePassword' element={<HOC Component={DoiMatKhau}></HOC>}></Route>
          <Route path='/quanTrong' element={<HOC Component={QuanTrong}></HOC>}></Route>
          <Route path='/chuY' element={<HOC Component={ChuY}></HOC>}></Route>
          <Route path='/tatCa' element={<HOC Component={TatCa}></HOC>}></Route>
          <Route path='/pdfViewer' element={<HOC Component={PDFViewer}></HOC>}></Route>
          <Route path='/chat' element={<HOC Component={ChatPage}></HOC>}></Route>
          <Route path='/chatNhom' element={<HOC Component={ChatNhom}></HOC>}></Route>
          <Route path='/addChat' element={<HOC Component={AddChat}></HOC>}></Route>
          <Route path='/taoNhom' element={<HOC Component={CreateGroup}></HOC>}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
